import { Component, Input } from "@angular/core";
import { BankAccount } from "app/core/bank-account/bank-account.interface";

@Component({
  selector: "bank-accounts-overview",
  template: `
    <mat-card>
      <mat-card-header>
        <mat-card-title>Bank Accounts Overview:</mat-card-title>
      </mat-card-header>
      <mat-card-content>
        <ng-container *ngIf="bankAccounts?.length">
          <table mat-table [dataSource]="bankAccounts">
            <!-- id Column -->
            <ng-container matColumnDef="id">
              <th mat-header-cell *matHeaderCellDef>Id.</th>
              <td mat-cell *matCellDef="let element">{{ element.id }}</td>
            </ng-container>

            <!-- bank Column -->
            <ng-container matColumnDef="bank_name">
              <th mat-header-cell *matHeaderCellDef>Bank Name</th>
              <td mat-cell *matCellDef="let element">
                {{ element.bank_name }}
              </td>
            </ng-container>

            <!-- account holder Column -->
            <ng-container matColumnDef="account_holder_name">
              <th mat-header-cell *matHeaderCellDef>Account Holder</th>
              <td mat-cell *matCellDef="let element">
                {{ element.account_holder_name }}
              </td>
            </ng-container>

            <!-- sort code Column -->
            <ng-container matColumnDef="sort_code">
              <th mat-header-cell *matHeaderCellDef>Sort Code</th>
              <td mat-cell *matCellDef="let element">
                {{ element.sort_code | sortCode }}
              </td>
            </ng-container>

            <!-- account number Column -->
            <ng-container matColumnDef="account_number">
              <th mat-header-cell *matHeaderCellDef>Account Number</th>
              <td mat-cell *matCellDef="let element">
                {{ element.account_number }}
              </td>
            </ng-container>

            <!-- current value Column -->
            <ng-container matColumnDef="current_value">
              <th mat-header-cell *matHeaderCellDef>Current Value</th>
              <td mat-cell *matCellDef="let element">
                {{ element.current_value | number : "1.2-2" }}
              </td>
            </ng-container>

            <tr mat-header-row *matHeaderRowDef="displayedColumns"></tr>
            <tr mat-row *matRowDef="let row; columns: displayedColumns"></tr>
          </table>
        </ng-container>
      </mat-card-content>
    </mat-card>
  `,
  styleUrls: ["./bank-accounts-overview.component.scss"],
})
export class BankAccountsOverviewComponent {
  @Input() bankAccounts?: BankAccount[];
  displayedColumns: string[] = [
    "id",
    "bank_name",
    "account_holder_name",
    "sort_code",
    "account_number",
    "current_value",
  ];
}
