import { Component, Input } from "@angular/core";
import { Transaction } from "app/core/transaction/transaction.interface";

@Component({
  selector: "transactions-overview",
  template: `
    <mat-card>
      <mat-card-header>
        <mat-card-title>Transactions Overview:</mat-card-title>
      </mat-card-header>
      <mat-card-content>
        <ng-container *ngIf="transactions?.length">
          <table mat-table [dataSource]="transactions">
            <!-- id Column -->
            <ng-container matColumnDef="id">
              <th mat-header-cell *matHeaderCellDef>Id.</th>
              <td mat-cell *matCellDef="let element">{{ element.id }}</td>
            </ng-container>

            <!-- transaction_type Column -->
            <ng-container matColumnDef="transaction_type">
              <th mat-header-cell *matHeaderCellDef>Transaction Type</th>
              <td mat-cell *matCellDef="let element">
                {{ element.transaction_type }}
              </td>
            </ng-container>

            <!-- amount Column -->
            <ng-container matColumnDef="amount">
              <th mat-header-cell *matHeaderCellDef>Ammount</th>
              <td mat-cell *matCellDef="let element">
                {{ element.amount | number : "1.2-2" }}
              </td>
            </ng-container>

            <!-- source bank Column -->
            <ng-container matColumnDef="source_bank_account_details">
              <th mat-header-cell *matHeaderCellDef>Source Account</th>
              <td mat-cell *matCellDef="let element">
                {{ element.source_bank_account_details }}
              </td>
            </ng-container>

            <!-- target bank Column -->
            <ng-container matColumnDef="target_bank_account_details">
              <th mat-header-cell *matHeaderCellDef>Target Account</th>
              <td mat-cell *matCellDef="let element">
                {{ element.target_bank_account_details }}
              </td>
            </ng-container>

            <!-- description Column -->
            <ng-container matColumnDef="description">
              <th mat-header-cell *matHeaderCellDef>Description</th>
              <td mat-cell *matCellDef="let element">
                {{ element.description }}
              </td>
            </ng-container>

            <tr mat-header-row *matHeaderRowDef="displayedColumns"></tr>
            <tr mat-row *matRowDef="let row; columns: displayedColumns"></tr>
          </table>
        </ng-container>
      </mat-card-content>
    </mat-card>
  `,
  styleUrls: ["./transactions-overview.component.scss"],
})
export class TransactionsOverviewComponent {
  @Input() transactions?: Transaction[];
  displayedColumns: string[] = [
    "id",
    "transaction_type",
    "amount",
    "source_bank_account_details",
    "target_bank_account_details",
    "description",
  ];
}
