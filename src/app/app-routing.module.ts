import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardContainer } from './containers/dashboard/dashboard.container';
import { TransactionFormContainer } from './containers/transaction-form/transaction-form.component';
import { TransactionType } from './core/transaction/transaction.interface';

const routes: Routes = [
  {
    path: 'dashboard',
    component: DashboardContainer,
  },
  {
    path: 'transfer',
    component: TransactionFormContainer,
    data: {
      transactionType: TransactionType.TRANSFER,
    },
    pathMatch: 'full',
  },
  {
    path: 'deposit',
    component: TransactionFormContainer,
    data: {
      transactionType: TransactionType.DEPOSIT,
    },
    pathMatch: 'full',
  },
  {
    path: 'withdraw',
    component: TransactionFormContainer,
    data: {
      transactionType: TransactionType.WITHDRAW,
    },
    pathMatch: 'full',
  },
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  { path: '**', redirectTo: 'dashboard' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
