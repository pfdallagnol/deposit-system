import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "sortCode",
})
export class SortCodePipe implements PipeTransform {
  transform(sortCode: string): string {
    return (
      sortCode
        .replace(" ", "")
        .replace("-", "")
        .match(/.{1,2}/g) ?? []
    ).join("-");
  }
}
