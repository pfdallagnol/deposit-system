import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatTableModule } from '@angular/material/table';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { InputMaskModule } from '@ngneat/input-mask';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BankAccountsOverviewComponent } from './components/bank-accounts-overview/bank-accounts-overview.component';
import { TransactionsOverviewComponent } from './components/transactions-overview/transactions-overview.component';
import { DashboardContainer } from './containers/dashboard/dashboard.container';
import { TransactionFormContainer } from './containers/transaction-form/transaction-form.component';
import { SortCodePipe } from './pipes/sort-code.pipe';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatCardModule } from '@angular/material/card';

@NgModule({
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatInputModule,
    MatFormFieldModule,
    MatTableModule,
    InputMaskModule,
    MatAutocompleteModule,
    MatSnackBarModule,
    MatCheckboxModule,
    MatCardModule,
  ],
  declarations: [
    AppComponent,
    BankAccountsOverviewComponent,
    TransactionFormContainer,
    SortCodePipe,
    TransactionsOverviewComponent,
    DashboardContainer,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
