import { Component } from "@angular/core";
import { TransactionType } from "app/core/transaction/transaction.interface";
import { BankAccountStateService } from "app/services/bank-account-state.service";
import { TransactionStateService } from "app/services/transaction-state.service";

@Component({
  selector: "dashboard",
  template: `
    <div class="dashboard">
      <div class="transactions-actions">
        <button mat-button color="primary" routerLink="/deposit">
          New Deposit
        </button>
        <button mat-button color="primary" routerLink="/transfer">
          New Transfer
        </button>
        <button mat-button color="primary" routerLink="/withdraw">
          New Withdraw
        </button>
      </div>

      <div class="transactions">
        <transactions-overview
          [transactions]="transactions$ | async"
        ></transactions-overview>
      </div>

      <div class="accounts">
        <bank-accounts-overview
          [bankAccounts]="bankAccounts$ | async"
        ></bank-accounts-overview>
      </div>
    </div>
  `,
  styleUrls: ["./dashboard.container.scss"],
})
export class DashboardContainer {
  title = "Deposit System";
  transactionType = TransactionType;
  transactions$ =
    this.transactionStateService.selectTransactionsWithBankDetails();
  bankAccounts$ = this.bankAccountStateService.selectBankAccounts();

  constructor(
    private bankAccountStateService: BankAccountStateService,
    private transactionStateService: TransactionStateService
  ) {}
}
