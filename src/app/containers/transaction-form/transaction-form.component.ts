import { Component, Input, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { MatSnackBar } from "@angular/material/snack-bar";
import { ActivatedRoute, Router } from "@angular/router";
import { createMask } from "@ngneat/input-mask";
import { map, Observable, startWith, switchMap, take } from "rxjs";
import { BankAccount } from "app/core/bank-account/bank-account.interface";
import {
  TransactionCreate,
  TransactionType,
} from "app/core/transaction/transaction.interface";
import { BankAccountStateService } from "app/services/bank-account-state.service";
import { TransactionStateService } from "app/services/transaction-state.service";

@Component({
  selector: "transaction-form",
  template: `
    <h3>{{ formType }}</h3>
    <form
      *ngIf="transactionForm"
      [formGroup]="transactionForm"
      (keydown.enter)="$event.preventDefault()"
    >
      <div *ngIf="formType !== TransactionType.DEPOSIT" class="source-bank">
        <mat-form-field>
          <label>Source Bank:</label>
          <input
            matInput
            type="text"
            formControlName="source_bank_account"
            [matAutocomplete]="source"
            (blur)="onBankBlur('source_bank_account')"
          />
          <span>{{ (sourceBankAccount$ | async)?.account_holder_name }}</span>

          <mat-autocomplete
            #source="matAutocomplete"
            (optionSelected)="onSourceBankAccountSelected($event.option.value)"
          >
            <mat-option
              *ngFor="
                let sourceBankAccount of filteredSourceBankAccount$ | async
              "
              [value]="sourceBankAccount.id"
            >
              <span>
                {{ sourceBankAccount.id }} -
                {{ sourceBankAccount.account_holder_name }}
              </span>
            </mat-option>
          </mat-autocomplete>
        </mat-form-field>
      </div>

      <div *ngIf="formType !== TransactionType.WITHDRAW" class="target-bank">
        <mat-form-field>
          <label>Target Bank:</label>
          <input
            matInput
            type="text"
            formControlName="target_bank_account"
            [matAutocomplete]="target"
            (blur)="onBankBlur('target_bank_account')"
          />
          <span>{{ (targetBankAccount$ | async)?.account_holder_name }}</span>

          <mat-autocomplete
            #target="matAutocomplete"
            (optionSelected)="onTargetBankAccountSelected($event.option.value)"
          >
            <mat-option
              *ngFor="
                let targetBankAccount of filteredTargetBankAccount$ | async
              "
              [value]="targetBankAccount.id"
            >
              <span>
                {{ targetBankAccount.id }} -
                {{ targetBankAccount.account_holder_name }}
              </span>
            </mat-option>
          </mat-autocomplete>
        </mat-form-field>
      </div>

      <mat-checkbox
        *ngIf="formType === TransactionType.WITHDRAW"
        class="withdraw-all"
        formControlName="withdraw_all"
        (change)="onWithdrawAllCheck($event.checked)"
        >Would you like to withdraw the full value of this
        account?</mat-checkbox
      >

      <ng-container *ngIf="!transactionForm.get('withdraw_all').value">
        <span
          *ngIf="(sourceBankAccount$ | async)?.current_value as currentValue"
          >Amount available:
          {{ currentValue }}
        </span>

        <div class="amount">
          <mat-form-field>
            <label>Amount:</label>
            <input
              matInput
              type="text"
              [inputMask]="amountInputMask"
              formControlName="amount"
              (blur)="onAmountBlur()"
            />
          </mat-form-field>
        </div>
      </ng-container>

      <div class="description">
        <mat-form-field>
          <label>Description:</label>
          <input matInput type="text" formControlName="description" />
        </mat-form-field>
      </div>

      <div class="actions">
        <button mat-button color="primary" (click)="returnToDashBoard()">
          Cancel
        </button>
        <button
          mat-raised-button
          color="primary"
          type="button"
          [disabled]="!transactionForm.valid"
          (click)="onSave()"
        >
          Save
        </button>
      </div>
    </form>
  `,
  styleUrls: ["./transaction-form.component.scss"],
})
export class TransactionFormContainer implements OnInit {
  @Input()
  set transactionType(type: TransactionType) {
    this.formType = type;
    this.transactionForm = new FormGroup({
      transaction_type: new FormControl(type, Validators.required),
      amount: new FormControl(0, [Validators.required, Validators.min(0.01)]),
      source_bank_account: new FormControl(
        type !== TransactionType.DEPOSIT ? "" : null,
        type !== TransactionType.DEPOSIT ? Validators.required : undefined
      ),
      target_bank_account: new FormControl(
        type !== TransactionType.WITHDRAW ? "" : null,
        type !== TransactionType.WITHDRAW ? Validators.required : undefined
      ),
      description: new FormControl(""),
      withdraw_all: new FormControl(false),
    });
  }

  formType?: TransactionType;
  TransactionType = TransactionType;
  transactionForm?: FormGroup;

  amountInputMask = createMask({
    alias: "numeric",
    digits: 2,
    digitsOptional: false,
    placeholder: "0",
  });

  bankInputMask = createMask({
    alias: "numeric",
    rightAlign: false,
    placeholder: "0",
  });

  sourceBankAccount$?: Observable<BankAccount | undefined>;
  filteredSourceBankAccount$?: Observable<BankAccount[]>;
  targetBankAccount$?: Observable<BankAccount | undefined>;
  filteredTargetBankAccount$?: Observable<BankAccount[]>;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private transactionStateService: TransactionStateService,
    private bankAccountStateService: BankAccountStateService,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    if (this.route.snapshot.data?.transactionType) {
      this.transactionType = this.route.snapshot.data.transactionType;
    }

    this.filteredSourceBankAccount$ = this.transactionForm
      ?.get("source_bank_account")
      ?.valueChanges.pipe(
        startWith(""),
        switchMap((value) =>
          this.bankAccountStateService.selectBankAccountsByContainFilter(
            value || ""
          )
        )
      );

    this.filteredTargetBankAccount$ = this.transactionForm
      ?.get("target_bank_account")
      ?.valueChanges.pipe(
        startWith(""),
        switchMap((value) =>
          this.bankAccountStateService.selectBankAccountsByContainFilter(
            value || ""
          )
        )
      );
  }

  onSourceBankAccountSelected(bankAccountId: number): void {
    this.sourceBankAccount$ =
      this.bankAccountStateService.selectBankAccountsById(+bankAccountId);
  }

  onTargetBankAccountSelected(bankAccountId: number): void {
    this.targetBankAccount$ =
      this.bankAccountStateService.selectBankAccountsById(+bankAccountId);
  }

  onBankBlur(controlName: string): void {
    if (
      this.transactionForm?.get(controlName)?.value === "" &&
      controlName === "source_bank_account"
    ) {
      this.sourceBankAccount$ =
        this.bankAccountStateService.selectBankAccountsById(
          this.transactionForm.get(controlName)?.value
        );
      return;
    }

    if (
      this.transactionForm?.get(controlName)?.value === "" &&
      controlName === "target_bank_account"
    ) {
      this.targetBankAccount$ =
        this.bankAccountStateService.selectBankAccountsById(
          this.transactionForm.get(controlName)?.value
        );
      return;
    }
  }

  onAmountBlur(): void {
    this.sourceBankAccount$?.pipe(take(1)).subscribe((account) => {
      if (
        account &&
        +this.transactionForm?.get("amount")?.value > account.current_value
      ) {
        this.transactionForm?.get("amount")?.patchValue(0);
        this.snackBar.open("Amount should not exceed the available", "Ok");
      }
    });
  }

  onWithdrawAllCheck(checked: boolean): void {
    this.sourceBankAccount$?.pipe(take(1)).subscribe((account) => {
      if (account && checked) {
        this.transactionForm?.get("amount")?.patchValue(account.current_value);
      } else {
        this.transactionForm?.get("amount")?.patchValue(0);
      }
    });
  }

  onSave(): void {
    if (
      !this.transactionForm ||
      this.transactionForm?.invalid ||
      !this.formType
    ) {
      return;
    }

    const transactionToCreate: TransactionCreate = {
      transaction_type: this.formType,
      amount: this.transactionForm.get("amount")?.value,
      source_bank_account_id: this.transactionForm.get("source_bank_account")
        ?.value,
      target_bank_account_id: this.transactionForm.get("target_bank_account")
        ?.value,
      description: this.transactionForm.get("description")?.value,
    };

    this.transactionStateService.createTransaction(transactionToCreate);
    this.returnToDashBoard();
  }

  returnToDashBoard(): void {
    this.router.navigate(["/dashboard"]);
  }
}
