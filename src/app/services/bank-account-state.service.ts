import { Injectable } from "@angular/core";
import { BehaviorSubject, map, Observable } from "rxjs";
import { BankAccount } from "../core/bank-account/bank-account.interface";
import { BANK_ACCOUNTS } from "../core/bank-account/bank-account.stub";
import { TransactionType } from "../core/transaction/transaction.interface";

export interface BankAccountState {
  bankAccounts: BankAccount[];
}

@Injectable({
  providedIn: "root",
})
export class BankAccountStateService {
  private state$ = new BehaviorSubject<BankAccountState>({
    bankAccounts: BANK_ACCOUNTS,
  });

  private patchState(state: Partial<BankAccountState>) {
    this.state$.next({
      ...this.state$.getValue(),
      ...state,
    });
  }

  selectBankAccounts(): Observable<BankAccount[]> {
    return this.state$.pipe(map((state) => state.bankAccounts));
  }

  addTransactionToBankAccount(
    bankAccountId: number,
    type: TransactionType.DEPOSIT | TransactionType.WITHDRAW,
    amount: number
  ): void {
    const bankAccounts = this.state$.getValue().bankAccounts;
    const account = bankAccounts.find(
      (account) => account.id === bankAccountId
    );

    if (!account) {
      return;
    }
    account.current_value =
      type === TransactionType.WITHDRAW
        ? account.current_value - +amount
        : account.current_value + +amount;

    this.patchState({ bankAccounts: bankAccounts });
  }

  selectBankAccountsById(query: number): Observable<BankAccount | undefined> {
    return this.state$.pipe(
      map((state) =>
        state.bankAccounts.find((bankAccount) => bankAccount.id === query)
      )
    );
  }

  selectBankAccountsByIdSnapShot(query: number): BankAccount | undefined {
    return this.state$
      .getValue()
      .bankAccounts.find((bankAccount) => bankAccount.id === query);
  }

  selectBankAccountsByContainFilter(query: string): Observable<BankAccount[]> {
    return this.state$.pipe(
      map((state) => {
        if (!query) {
          return state.bankAccounts;
        } else {
          return state.bankAccounts.filter(
            (bankAccount) =>
              bankAccount.id.toString().includes(query) ||
              bankAccount.account_holder_name.includes(query)
          );
        }
      })
    );
  }
}
