import { Injectable } from "@angular/core";
import { BehaviorSubject, map, Observable } from "rxjs";
import {
  Transaction,
  TransactionCreate,
  TransactionOverview,
  TransactionType,
} from "../core/transaction/transaction.interface";
import { TRANSACTIONS } from "../core/transaction/transaction.stub";
import { BankAccountStateService } from "./bank-account-state.service";

export interface TransactionState {
  transactions: Transaction[];
}

@Injectable({
  providedIn: "root",
})
export class TransactionStateService {
  constructor(private bankAccountStateService: BankAccountStateService) {}

  private state$ = new BehaviorSubject<TransactionState>({
    transactions: TRANSACTIONS,
  });

  selectTransactions(): Observable<Transaction[]> {
    return this.state$.pipe(map((state) => state.transactions));
  }

  selectTransactionsWithBankDetails(): Observable<TransactionOverview[]> {
    return this.state$.pipe(
      map((state) =>
        state.transactions.map((transaction) => {
          const sourceBankAccount =
            this.bankAccountStateService.selectBankAccountsByIdSnapShot(
              transaction.source_bank_account_id as number
            );
          const targetBankAccount =
            this.bankAccountStateService.selectBankAccountsByIdSnapShot(
              transaction.target_bank_account_id as number
            );

          return {
            id: transaction.id,
            transaction_type: transaction.transaction_type,
            amount: transaction.amount,
            source_bank_account_details: sourceBankAccount
              ? `${sourceBankAccount.id} - ${sourceBankAccount.account_holder_name}`
              : "",
            target_bank_account_details: targetBankAccount
              ? `${targetBankAccount.id} - ${targetBankAccount.account_holder_name}`
              : "",
            description: transaction.description,
          };
        })
      )
    );
  }

  private patchState(state: Partial<TransactionState>) {
    this.state$.next({
      ...this.state$.getValue(),
      ...state,
    });
  }

  createTransaction(transaction: TransactionCreate): void {
    const currentState = this.state$.getValue();

    const newTransaction: Transaction = {
      id: currentState.transactions.length + 1,
      ...transaction,
    };

    this.patchState({
      transactions: [...currentState.transactions, newTransaction],
    });

    switch (transaction.transaction_type) {
      case TransactionType.TRANSFER: {
        this.bankAccountStateService.addTransactionToBankAccount(
          transaction.source_bank_account_id,
          TransactionType.WITHDRAW,
          transaction.amount
        );
        this.bankAccountStateService.addTransactionToBankAccount(
          transaction.target_bank_account_id,
          TransactionType.DEPOSIT,
          transaction.amount
        );
        break;
      }
      case TransactionType.DEPOSIT: {
        this.bankAccountStateService.addTransactionToBankAccount(
          transaction.target_bank_account_id,
          TransactionType.DEPOSIT,
          transaction.amount
        );
        break;
      }
      case TransactionType.WITHDRAW: {
        this.bankAccountStateService.addTransactionToBankAccount(
          transaction.source_bank_account_id,
          TransactionType.WITHDRAW,
          transaction.amount
        );
      }
    }
  }
}
